///////////////
// Sketch: Calibrate - Calibrate TFT SPFD5408 Touch
// Author: Joao Lopes F. - joaolopesf@gmail.com
//
// Versions:
//    - 0.9.0  First beta - July 2015
//    - 0.9.1  Rotation for Mega
// Comments:
//    Show the calibration parameters to put in your code
//    Please use a small like the eraser on a pencil for best results
//
//    Code for buttons, based on Adafruit arduin_o_phone example
///////////////

// library SPFD5408

#include <SPFD5408_Adafruit_GFX.h>    // Core graphics library
#include <SPFD5408_Adafruit_TFTLCD.h> // Hardware-specific library
#include <SPFD5408_TouchScreen.h>     // Touch library


#if defined(__SAM3X8E__)
#undef __FlashStringHelper::F(string_literal)
#define F(string_literal) string_literal
#endif

// Calibrates value
#define SENSIBILITY 300
#define MINPRESSURE 10
#define MAXPRESSURE 1000

//These are the pins for the shield!
#define YP A1
#define XM A2
#define YM 7
#define XP 6

//Macros replaced by variables
/*  #define TS_MINX 150
  #define TS_MINY 120
  #define TS_MAXX 920
  #define TS_MAXY 940
*/
///////////////////// VALEUR CALIBRE
//Macros replaced by variables
#define TS_MINX 108
#define TS_MINY 82
#define TS_MAXX 940
#define TS_MAXY 912

// Init TouchScreen:
TouchScreen ts = TouchScreen(XP, YP, XM, YM, SENSIBILITY);

// LCD Pin
#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
#define LCD_RESET A4 // Optional : otherwise connect to Arduino's reset pin

// Init LCD
Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define ANALOG_PORT 5
#define ANALOG_ALIM 6

/////////////////////////////// DEFINE   GUI /////////////
#define ECRAN_L 240
#define ECRAN_H 320

#define GUI_LIGHT WHITE
#define GUI_DARK DARKGREY
#define GUI_FOND LIGHTGREY

#define GUI_MARGE 3

#define TITRE_BAR_H 15
#define TITRE_TXT_SIZE 1

#define PUI_GUI_X GUI_MARGE
#define PUI_GUI_Y (TITRE_BAR_H + GUI_MARGE)
#define PUI_GUI_L (ECRAN_L- 2*GUI_MARGE)
#define PUI_TXT_TAILLE 1
#define PUI_TXT_COLOR  GUI_DARK
#define PUI_VAL_TAILLE_DB 3
#define PUI_VAL_COLOR  BLACK
#define PUI_VAL_TAILLE_W 2
#define PUI_VAL_TAILLE_V 2
#define PUI_GUI_H 81  //  1*7 + 3*7 + 2*7 + 2*7 + 4*5 + 5 = PUI_GUI_Y + PUI_VAL_TAILLE_DB * 7 + PUI_VAL_TAILLE_W * 7  + 7 * PUI_TXT_TAILLE + 4 * GUI_MARGE

#define BUTTONS  6
#define BUTTON_L 75
#define BUTTON_H 35
#define BUTTON_MARGE ((ECRAN_L - ( 3 * BUTTON_L) )/ 4)
#define BUTTON_X ((BUTTON_L / 2) + BUTTON_MARGE)
#define BUTTON_Y (ECRAN_H - BUTTON_H*1.5 - GUI_MARGE*2 )
#define BUTTON_TXT 1

#define MOY_GUI_L (BUTTON_L *2)
#define MOY_GUI_H BUTTON_H
#define MOY_GUI_X (BUTTON_X + (BUTTON_L/2 + BUTTON_MARGE))
#define MOY_GUI_Y (BUTTON_Y - BUTTON_H /2 - MOY_GUI_H  - GUI_MARGE)
#define MOY_VAL_TAILLE 2
#define MOY_TXT_COLOR BLACK
#define MOY_TXT_TAILLE 1

#define FREQ_GUI_L (BUTTON_L)
#define FREQ_GUI_H BUTTON_H
#define FREQ_GUI_X (BUTTON_X - BUTTON_L/2)
#define FREQ_GUI_Y (BUTTON_Y - BUTTON_H /2 - MOY_GUI_H  - GUI_MARGE)
#define FREQ_VAL_TAILLE 2
#define FREQ_TXT_COLOR BLACK
#define FREQ_TXT_TAILLE 1


#define BUTTON_FREQ   0
#define BUTTON_MOY_M  1
#define BUTTON_MOY_P  2
#define BUTTON_ENV    3
#define BUTTON_MODE   4
#define BUTTON_MESU   5

Adafruit_GFX_Button buttons[BUTTONS];

#define GRAPH_GUI_X GUI_MARGE
#define GRAPH_GUI_Y (PUI_GUI_Y + PUI_GUI_H + GUI_MARGE)
#define GRAPH_GUI_L (ECRAN_L- 2*GUI_MARGE)
#define GRAPH_GUI_H (MOY_GUI_Y - GRAPH_GUI_Y - GUI_MARGE)
#define GRAPH_GUI_FOND_COLOR  BLACK
#define GRAPH_GUI_CADRE_COLOR  GUI_LIGHT
#define GRAPH_TXT_TAILLE 1
#define GRAPH_TXT_COLOR  GUI_LIGHT
#define GRAPH_VAL_X (GRAPH_GUI_X + GUI_MARGE +  10*3*GRAPH_TXT_TAILLE )
#define GRAPH_VAL_Y (GRAPH_GUI_Y + 7*GRAPH_TXT_TAILLE + GUI_MARGE)
#define GRAPH_VAL_L (GRAPH_GUI_L - GRAPH_VAL_X  - 2*GUI_MARGE)
#define GRAPH_VAL_H (GRAPH_GUI_H - 7*GRAPH_TXT_TAILLE - GUI_MARGE*2)

#define FREQ_MAX 6 // nombre de bande

//////////////////////////////////// Variable global
//char buttons_y = 0;
//uint16_t buttons_y = 0;


// Dimensions
byte width = 0;
byte height = 0;
//uint16_t width = 0;
//uint16_t height = 0;

float Tension, P_dBm, P_W;
float P_dBm_old = 0, P_W_old = 0, Tension_old = 0;
boolean mesure = false;
boolean graph = false;
boolean mode = false;
boolean envoie = false;
byte    moyennage = 0;

double P_dBm_Tab[GRAPH_VAL_L];
byte    freq = 0 ;
// 0:7  Mhz
// 1:14 MHz
// 2:21 MHz
// 3:28 MHz
// 4:144 MHz
// 5:432 MHz
// 6:1000 MHz
char freq_tab[FREQ_MAX+1][6]   = {"7",    "14",  "21",  "28",  "144", "432", "1000"};
//int freq_coef_a[FREQ_MAX+1] = { 40, 40, 40, 40, 40, 39, 35 };
//int freq_coef_b[FREQ_MAX+1] = { 88, 88, 88, 87, 86, 79,  60    };
float freq_coef_a[FREQ_MAX+1] = { 40.03, 40.08, 40.08, 40.12, 40.12, 38.95, 34.66 };
float freq_coef_b[FREQ_MAX+1] = { 87.54, 87.59, 87.47, 87.37, 85.85, 78.9,  60    };

///////////////////////////////// Setup
void setup(void) {

  // Serial por for debug, not works if shield is plugged in arduino

  Serial.begin(9600);

  // Inicialize the controller

  tft.reset();

  tft.begin(0x9341);

  tft.setRotation(0); // Need for the Mega, please changed for your choice or rotation initial

  width = tft.width() - 1;
  height = tft.height() - 1;

  // Debug

  /*Serial.println(F("TFT LCD test"));
  Serial.print("TFT size is ");
  Serial.print(tft.width());
  Serial.print("x");
  Serial.println(tft.height());
*/
  Serial.println(F("Powermeter - F4HDG -"));
  
  // UI

  initializeButtons();

  // Border

  showAbout();

  // Initial screen

  delay(1000);
  //  waitOneTouch();

  showGUI();
}

///////////////////////////////////////////// Loop
void loop()
{
  TSPoint p;
  float puissance;
  int i;
  String instruction;
  // Wait a touch
  /*  digitalWrite(13, HIGH);
    // p = waitOneTouch();
    digitalWrite(13, LOW);
  */
  digitalWrite(13, HIGH);
  p = ts.getPoint();
  pinMode(XM, OUTPUT); //Pins configures again for TFT control
  pinMode(YP, OUTPUT);
  digitalWrite(13, LOW);

  if ((p.z > MINPRESSURE ) && (p.z < MAXPRESSURE))
  {
    p.x = mapXValue(p);
    p.y = mapYValue(p);

    //DEGUG
    //tft.fillCircle(p.x, p.y, 3, BLUE);
    // showTouched(p);

    // APPUIE BOUTTONS ?????????????
    for (uint8_t b = 0; b < BUTTONS; b++)
    {
      if (buttons[b].contains(p.x, p.y))
      {
        switch  (b) {
          case BUTTON_MODE:
            buttons[b].drawButton(false);
            // MODE SIMPLE // CONTINUE
            if (mode)
            {
              buttons[b].drawButton(false);
              mode = false;
            }
            else
            {
              buttons[b].drawButton(true);
              mode = true;
            }
            break;

          case BUTTON_MOY_M:
            buttons[b].drawButton(true);
            // - MOYENNAGE
            if (moyennage > 0)
            {
              Txt_Value_Moyenne(GUI_LIGHT);
              moyennage = moyennage - 1;
              Txt_Value_Moyenne(GUI_DARK);
            }
            buttons[b].drawButton(false);
            break;

          case BUTTON_MOY_P:
            buttons[b].drawButton(true);
            // + MOYENNAGE
            if (moyennage < 10)
            {
              Txt_Value_Moyenne(GUI_LIGHT);
              moyennage = moyennage + 1;
              Txt_Value_Moyenne(GUI_DARK);
            }
            buttons[b].drawButton(false);
            break;

          case BUTTON_ENV:
            // ENVOIE Sur le port srie
            if (envoie)
            {
              buttons[b].drawButton(false);
              envoie = false;
            }
            else
            {
              buttons[b].drawButton(true);
              envoie = true;
            }
            break;

          case BUTTON_FREQ:

            Txt_Value_Frequence(GUI_LIGHT);
            freq = freq + 1;
            if (freq > FREQ_MAX)
              {
              freq = 0;
              }
            Txt_Value_Frequence(GUI_DARK);
            break;

          case BUTTON_MESU:
            // Mesurer
            if (mesure)
            {
              buttons[b].drawButton(false);
              mesure = false;
            }
            else
            {
              buttons[b].drawButton(true);
              mesure = true;
            }
            break;
        }
      }
      //Serial.println(p.y);
      if (p.y <= TITRE_BAR_H)
      {
        showAbout();
        waitOneTouch();
        showGUI();
        Txt_Value_Puissance();
        Txt_Value_Moyenne(GUI_DARK);
        drawGraph();
        break;
      }

      if (p.y > PUI_GUI_Y && p.y < PUI_GUI_Y + PUI_GUI_H )
      {
        if (mesure)
        {
          buttons[BUTTON_MESU].drawButton(false);
          mesure = false;
        }
        else
        {
          buttons[BUTTON_MESU].drawButton(true);
          mesure = true;
        }
        break;
      }
      // BUTTON_GRAF
      if (p.y > GRAPH_GUI_Y && p.y < GRAPH_GUI_Y + GRAPH_GUI_H )
      {
        if (graph)
          graph = false;
        else
          graph = true;
        break;

      }
    }
    delay(500);
  }

  if (mesure)
  {
    puissance = Mesure_Puissance();
    Txt_Value_Puissance();

    if (graph)
    {
      for (i = 0; i < GRAPH_VAL_L - 1; i++)
      {
        P_dBm_Tab[i] = P_dBm_Tab[i + 1];
      }
      P_dBm_Tab[i] = puissance;
      drawGraph();
    }
  }

  if (!mode && mesure)
  {
    mesure = false;
    buttons[5].drawButton(false);
  }
  if (envoie && mesure)
  {
    //Serial.print("Puissance ");
    Serial.println(P_dBm);
    //Serial.println("dBm");
  }

  if (Serial.available() > 0)
  {
    instruction = Serial.readStringUntil(';');
    if (instruction == "PDBM" || instruction == "pdbm")
    {
      Mesure_Puissance();
      Txt_Value_Puissance();
      Serial.print("Puissance ");
      Serial.print(P_dBm);
      Serial.println(" dBm");
    }
    else if (instruction == "PW" || instruction == "pw")
    {
      Mesure_Puissance();
      Txt_Value_Puissance();
      Serial.print("Puissance ");
      if (P_W >= 1)
      {
        Serial.print(P_W);
        Serial.println(" mW");
      }
      else if (P_W * 1000 >= 1)
      {
        Serial.print(P_W * 1000);
        Serial.println(" uW");
      }
      else
      {
        Serial.print(P_W * 1000 * 1000);
        Serial.println(" nW");
      }
    }
    else if (instruction == "VOLT" || instruction == "volt")
    {
      Mesure_Puissance();
      Txt_Value_Puissance();
      Serial.print("Tension ");
      Serial.print(Tension);
      Serial.println(" V");
    }
    else
    {
      Serial.print(instruction);
      Serial.println(" : Instruction inconnue");
    }
  }

}

//////////////////////////////////////////////// MESURE PUISSANCE
double Mesure_Puissance()
{
  int Analog;
  char i;

  P_dBm_old = P_dBm;
  P_W_old = P_W;
  Tension_old = Tension;

  Analog = Analog + analogRead(ANALOG_PORT);
  for (i = 1; i < pow(2, moyennage); i++)
  {
    Analog = Analog + analogRead(ANALOG_PORT);
    Analog = Analog / 2;
  }

  //Analog = Analog /moyennage;


  Tension = (Analog * 5.0) / 1024;

  //Tension = (Analog * 4.78) / 1024;
  //P_dBm = 40.75 * Tension - 91;
  P_dBm = freq_coef_a[freq] * Tension - freq_coef_b[freq];
  
  P_W = pow(10, P_dBm / 10.0);

  return P_dBm;
}

//////////////////////////////////////////////// AFFICHE PUISSANCE
void Txt_Value_Puissance()
{
  char  large;
  //////  Puissance
  if (P_dBm_old < -99)
    large = 7;
  else if (P_dBm_old < -9)
    large = 6;
  else if (P_dBm_old < 0)
    large = 5;
  else if (P_dBm_old < 9)
    large = 4;
  else
    large = 5;

  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - ((4 + large) * 3 * PUI_VAL_TAILLE_DB) , PUI_GUI_Y + 7 * PUI_TXT_TAILLE + 2 * GUI_MARGE);
  tft.setTextColor(GUI_LIGHT);
  tft.setTextSize(PUI_VAL_TAILLE_DB);
  tft.print(P_dBm_old);
  tft.print(" dBm");

  if (P_dBm < -99)
    large = 7;
  else if (P_dBm < -9)
    large = 6;
  else if (P_dBm < 0)
    large = 5;
  else if (P_dBm < 9)
    large = 4;
  else
    large = 5;

  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - ((4 + large) * 3 * PUI_VAL_TAILLE_DB) , PUI_GUI_Y + 7 * PUI_TXT_TAILLE + 2 * GUI_MARGE);
  tft.setTextColor(PUI_VAL_COLOR);
  tft.setTextSize(PUI_VAL_TAILLE_DB);
  tft.print(P_dBm);
  tft.print(" dBm");

  ////// puissance W
  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - ((4 + 5) * 3 * PUI_VAL_TAILLE_W ), PUI_GUI_Y +  PUI_VAL_TAILLE_DB * 7  + 7 * PUI_TXT_TAILLE + 3 * GUI_MARGE);
  tft.setTextSize(PUI_VAL_TAILLE_W);
  tft.setTextColor(GUI_LIGHT);
  if (P_W_old >= 1)
  {
    tft.print(P_W_old);
    tft.print(" mW");
  }
  else if (P_W_old * 1000 >= 1)
  {
    tft.print(P_W_old * 1000);
    tft.print(" uW");
  }
  else
  {
    tft.print(P_W_old * 1000 * 1000);
    tft.print(" nW");
  }

  tft.setTextSize(PUI_VAL_TAILLE_W);
  tft.setTextColor(PUI_VAL_COLOR);
  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - ((4 + 5) * 3 * PUI_VAL_TAILLE_W ), PUI_GUI_Y +  PUI_VAL_TAILLE_DB * 7  + 7 * PUI_TXT_TAILLE + 3 * GUI_MARGE);
  if (P_W >= 1)
  {
    tft.print(P_W);
    tft.print(" mW");
  }
  else if (P_W * 1000 >= 1)
  {
    tft.print(P_W * 1000);
    tft.print(" uW");
  }
  else
  {
    tft.print(P_W * 1000 * 1000);
    tft.print(" nW");
  }

  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - (6 * 3 * PUI_VAL_TAILLE_W ), PUI_GUI_Y + PUI_VAL_TAILLE_DB * 7 + PUI_VAL_TAILLE_W * 7  + 7 * PUI_TXT_TAILLE + 4 * GUI_MARGE);
  tft.setTextSize(PUI_VAL_TAILLE_V);
  tft.setTextColor(GUI_LIGHT);
  tft.print(Tension_old);
  tft.println(" V");
  tft.setTextColor(PUI_VAL_COLOR);
  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - (6 * 3 * PUI_VAL_TAILLE_W ), PUI_GUI_Y + PUI_VAL_TAILLE_DB * 7 + PUI_VAL_TAILLE_W * 7  + 7 * PUI_TXT_TAILLE + 4 * GUI_MARGE);
  tft.print(Tension);
  tft.print(" V");

}

//////////////////////////////////////////////// AFFICHE MOYENNE
void Txt_Value_Moyenne(int color)
{
  byte large = 1;
  int moyenne = pow(2, moyennage);

  if (moyenne >= 10)
    large = 2;
  else if (moyenne >= 100)
    large = 3;
  if (moyenne >= 1000)
    large = 4;

  tft.setCursor(MOY_GUI_X + MOY_GUI_L / 2 - (4 + large * 3 * MOY_VAL_TAILLE ), MOY_GUI_Y + 4 * GUI_MARGE);
  tft.setTextColor(color);
  tft.setTextSize(MOY_VAL_TAILLE);
  tft.print(moyenne);
}

//////////////////////////////////////////////// AFFICHE FREQUENCE
void Txt_Value_Frequence(int color)
{
  /* int large = 1, moyenne = pow(2, moyennage);

   if (moyenne >= 10)
     large = 2;
   else if (moyenne >= 100)
     large = 3;
   if (moyenne >= 1000)
     large = 4;
  */
  //Serial.println(strlen(freq_tab[freq]));
  tft.setCursor(FREQ_GUI_X + (FREQ_GUI_L / 2) - (strlen(freq_tab[freq]) * 3 * FREQ_VAL_TAILLE), FREQ_GUI_Y + 4 * GUI_MARGE);
  tft.setTextColor(color);
  tft.setTextSize(FREQ_VAL_TAILLE);
  tft.print(freq_tab[freq]);
}

//////////////////////////////////////////////// AFFICHE GRAPHIC
void drawGraph()
{
  byte i;
  float P_Y;
  tft.drawLine(GRAPH_VAL_X, GRAPH_VAL_Y, GRAPH_VAL_X, GRAPH_VAL_Y + GRAPH_VAL_H, GUI_LIGHT);
  for (i = 0; i < GRAPH_VAL_L - 1; i++)
  {
    P_Y = GRAPH_VAL_Y + 1 + ((((-(GRAPH_VAL_H)) / 55) * P_dBm_Tab[i]) + 15.81);
    if (P_Y < GRAPH_VAL_Y + 1) P_Y = GRAPH_VAL_Y + 1;
    if (P_Y > GRAPH_VAL_Y + 1 + GRAPH_VAL_H - 2) P_Y = GRAPH_VAL_Y + 1 + GRAPH_VAL_H - 3;

    tft.drawPixel(GRAPH_VAL_X + i + 1, GRAPH_VAL_Y + 16 , WHITE);
    tft.drawPixel(GRAPH_VAL_X + i + 1, P_Y, BLACK);
    tft.drawPixel(GRAPH_VAL_X + i, P_Y, RED);
  }

  //    Serial.print("Y:");
  //    Serial.println(P_dBm_Tab[i]);
}

//////////////////////////////////////////////// AFFICHE GUI
void showGUI()
{
  // Clear
  tft.fillScreen(GUI_FOND);
  tft.setTextSize (TITRE_TXT_SIZE);
  // Header
  tft.drawRect(0, 0, width,   TITRE_BAR_H   , WHITE );
  tft.drawRect(1, 1, width - 1, TITRE_BAR_H - 1, BLACK);
  tft.fillRect(1, 1, width - 2, TITRE_BAR_H - 2, DARKGREEN);

  tft.setCursor (40, 3);
  tft.setTextColor(BLACK);
  tft.println("*** Puissance metre - F4HDG ***");

  // Footer

  TSPoint p; // Only for show initial values
  p.x = 0;
  p.y = 0;
  p.z = 0;

  tft.fillRoundRect(PUI_GUI_X, PUI_GUI_Y, PUI_GUI_L, PUI_GUI_H, min(PUI_GUI_L, PUI_GUI_H) / 5, GUI_LIGHT);
  tft.drawRoundRect(PUI_GUI_X, PUI_GUI_Y, PUI_GUI_L, PUI_GUI_H, min(PUI_GUI_L, PUI_GUI_H) / 5, GUI_DARK);
  tft.setCursor(PUI_GUI_X + PUI_GUI_L / 2 - strlen("Puissance") * 3 * PUI_TXT_TAILLE , PUI_GUI_Y + GUI_MARGE);
  tft.setTextColor(PUI_TXT_COLOR);
  tft.setTextSize(PUI_TXT_TAILLE);
  tft.print("Puissance");

  //  Moyennage
  tft.fillRoundRect(MOY_GUI_X, MOY_GUI_Y, MOY_GUI_L, MOY_GUI_H, min(MOY_GUI_L, MOY_GUI_H) / 5, GUI_LIGHT);
  tft.drawRoundRect(MOY_GUI_X, MOY_GUI_Y, MOY_GUI_L, MOY_GUI_H, min(MOY_GUI_L, MOY_GUI_H) / 5, GUI_DARK);
  tft.setCursor(MOY_GUI_X + MOY_GUI_L / 2 - strlen("Moyennage") * 3 * MOY_TXT_TAILLE , MOY_GUI_Y + GUI_MARGE);
  tft.setTextColor(MOY_TXT_COLOR);
  tft.setTextSize(MOY_TXT_TAILLE);
  tft.print("Moyennage");

  //  Frequence
  tft.fillRoundRect(FREQ_GUI_X, FREQ_GUI_Y, FREQ_GUI_L, FREQ_GUI_H, min(FREQ_GUI_L, FREQ_GUI_H) / 5, GUI_LIGHT);
  tft.drawRoundRect(FREQ_GUI_X, FREQ_GUI_Y, FREQ_GUI_L, FREQ_GUI_H, min(FREQ_GUI_L, FREQ_GUI_H) / 5, GUI_DARK);
  tft.setCursor(FREQ_GUI_X + FREQ_GUI_L / 2 - strlen("Frequence") * 3 * FREQ_TXT_TAILLE , FREQ_GUI_Y + GUI_MARGE);
  tft.setTextColor(FREQ_TXT_COLOR);
  tft.setTextSize(FREQ_TXT_TAILLE);
  tft.print("Frequence");

  Txt_Value_Puissance();
  Txt_Value_Moyenne(GUI_DARK);
  Txt_Value_Frequence(GUI_DARK);

  ///// GUI GRAPHx
  tft.fillRoundRect(GRAPH_GUI_X, GRAPH_GUI_Y, GRAPH_GUI_L, GRAPH_GUI_H, min(GRAPH_GUI_L, GRAPH_GUI_H) / 5, GRAPH_GUI_FOND_COLOR);
  tft.drawRoundRect(GRAPH_GUI_X, GRAPH_GUI_Y, GRAPH_GUI_L, GRAPH_GUI_H, min(GRAPH_GUI_L, GRAPH_GUI_H) / 5, GRAPH_GUI_CADRE_COLOR);
  tft.setCursor(GRAPH_GUI_X + GRAPH_GUI_L / 2 - strlen("Graphique") * 3 * GRAPH_TXT_TAILLE , GRAPH_GUI_Y + GUI_MARGE);
  tft.setTextColor(GRAPH_TXT_COLOR);
  tft.setTextSize(GRAPH_TXT_TAILLE);
  tft.print("Graphic");

  tft.drawLine(GRAPH_VAL_X, GRAPH_VAL_Y, GRAPH_VAL_X, GRAPH_VAL_Y + GRAPH_VAL_H, GUI_LIGHT);
  //tft.drawLine(GRAPH_VAL_X, GRAPH_VAL_Y + ((GRAPH_VAL_H) / 2), GRAPH_VAL_X + GRAPH_VAL_L, GRAPH_VAL_Y + ((GRAPH_VAL_H) / 2), GUI_LIGHT);
  tft.drawLine(GRAPH_VAL_X, GRAPH_VAL_Y + 16, GRAPH_VAL_X + GRAPH_VAL_L, GRAPH_VAL_Y + 16, GUI_LIGHT);
  tft.setCursor(GRAPH_GUI_X + GUI_MARGE, GRAPH_VAL_Y);
  tft.print("+15dB");
  tft.setCursor(GRAPH_GUI_X + GUI_MARGE, GRAPH_VAL_Y + 16);
  tft.print("0dB");
  tft.setCursor(GRAPH_GUI_X + GUI_MARGE, GRAPH_VAL_Y + (GRAPH_VAL_H) - 7);
  tft.print("-40dB");


  //DEBUG POS
  // showTouched(p);
  // Buttons
  for (uint8_t i = 0; i < BUTTONS; i++) {
    buttons[i].drawButton();
  }

}


//////////////////////////////////////////////// INIT BOUTTON
void initializeButtons() {


  char buttonlabels[6][10] = {"Frequence", " - ", " + ", "Envoie", "Continue", "Mesure"};
  //uint16_t buttoncolors[15] = {RED, BLUE, RED};

  for (uint8_t b = 0; b < BUTTONS / 2; b++) {
    buttons[b].initButton(&tft,                           // TFT object
                          BUTTON_X + b * (BUTTON_L + BUTTON_MARGE), BUTTON_Y,            // x, y,
                          BUTTON_L, BUTTON_H, GUI_DARK, GUI_LIGHT, BLACK,    // w, h, outline, fill,
                          buttonlabels[b], BUTTON_TXT);             // text
  }

  for (uint8_t b = BUTTONS / 2; b < BUTTONS; b++) {
    buttons[b].initButton(&tft,                           // TFT object
                          BUTTON_X + (b - 3) * (BUTTON_L + BUTTON_MARGE),  BUTTON_Y + BUTTON_MARGE + BUTTON_H,      // x, y,
                          BUTTON_L, BUTTON_H, GUI_DARK, GUI_LIGHT, BLACK,    // w, h, outline, fill,
                          buttonlabels[b], BUTTON_TXT);             // text
  }

  // Save the y position to avoid draws

  //buttons_y = BUTTON_Y;

}


//////////////////////////////////////////////// AFFICHE ABOUT
void showAbout () {

  unsigned char width = tft.width() - 1;
  unsigned char height = tft.height() - 1;

//  tft.fillScreen(DARKGREEN);
//  tft.fillRect(GUI_MARGE, GUI_MARGE, (width - GUI_MARGE * 2), (height - GUI_MARGE * 2), GUI_LIGHT);
//
//  tft.setCursor (width / 2 - (strlen("Puissance metre") * 6 )  , 50); // Taille de la chaine * 3 * Taille Police
//  tft.setTextSize (2);
//  tft.setTextColor(BLACK);
  tft.println("Puissance metre");
//  tft.setCursor (width / 2 - (strlen("- F4HDG -") * 6)  , 85);
  tft.println("- F4HDG -");
//  tft.setCursor (width / 2 - (strlen("Puissance metre") * 6 ) - 1 , 49); // Taille de la chaine * 3 * Taille Police
//  tft.setTextSize (2);
//  tft.setTextColor(GUI_DARK);
  tft.println("Puissance metre");
//  tft.setCursor (width / 2 - (strlen("- F4HDG -") * 6) - 1 , 84);
  tft.println("- F4HDG -");

//  tft.setCursor (width / 2 - (strlen("2015") * 6)  , 120);
//  tft.setTextSize (2);
//  tft.setTextColor(BLACK);
  tft.println("2015");
//  tft.setCursor (width / 2 - (strlen("Touch to proceed") * 3)  , 250);
//  tft.setTextSize (1);
//  tft.setTextColor(BLACK);
//  //  tft.println("Touch to proceed");

}



//////////////////////////////////////////////// ATTENDE PRESSION TACTIL
TSPoint waitOneTouch() {

  TSPoint p;

  do {
    p = ts.getPoint();

    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);

  } while ((p.z < MINPRESSURE ) || (p.z > MAXPRESSURE));

  return p;
}

//////////////////////////////////////////////// AFFICHE POSITION
//void showTouched(TSPoint p) {
//
//  uint8_t w = 40; // Width
//  uint8_t h = 10; // Heigth
//  uint8_t x = (width - (w * 2)); // X
//  uint8_t y = 11; // Y
//
//  tft.fillRect(x, y, w * 2, h, WHITE); // For cleanup
//
//  tft.drawRect(x, y, w, h, RED); // For X
//  tft.drawRect(x + w + 2, y, w * 2, h, RED); // For Y
//
//  tft.setTextSize(1);
//  tft.setTextColor(BLACK);
//  tft.setCursor(x + 2, y + 1);
//  tft.print("X: ");
//  showValue(p.x);
//
//  tft.setCursor(x + 2 + w + 2, y + 1);
//  tft.print("Y: ");
//  showValue(p.y);
//
//}

//void showValue (uint16_t value) {
//
//  if (value < 10)
//    tft.print("00");
//  if (value < 100)
//    tft.print("0");
//
//  tft.print(value);
//
//}
//////////////////////////////////////////////// MAP VELUR X ET Y
uint16_t mapXValue(TSPoint p) {
  uint16_t x = map(p.x, TS_MINX, TS_MAXX, 0, tft.width());
  return x;
}
uint16_t mapYValue(TSPoint p) {
  uint16_t y = map(p.y, TS_MINY, TS_MAXY, 0, tft.height());
  return y;
}
