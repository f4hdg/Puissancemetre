/*
 * Creation d'une unité de mesure de puissance
 * en mode puissance metre ou ROSmetre
 * Affichage puissance direct et reflechie + ROS
 * 
 */

#include <SPI.h>
#include <Adafruit_PCD8544.h>
#include <Encoder.h>

//////////////////////////////////////////////////////////////////////////// ECRAN
// Software SPI (slower updates, more flexible pin options):
// pin 13 - Serial clock out (SCLK)
// pin 11 - Serial data out (DIN)
// pin 10 - Data/Command select (D/C)
// pin 9 - LCD chip select (CS)
// pin 8 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(13, 11, 10, 9, 8);

//////////////////////////////////////////////////////////////////////////// Encoder
#define CODER_INTER  2

Encoder myEnc(3,4);
long myEnc_value_old;

//////////////////////////////////////////////////////////////////////////// MESURE ANALOG
#define DIRECT A0
#define RETOUR A1

double coupleur_dB = 20;

//////////////////////////////////////////////////////////////////////////// DETECTEUR
#define DETC_MAX 3
double detecteur_val = 0.60;
int detecteur_type = 0;

//////////////////////////////////////////////////////////////////////////// GESTION LOOP
int  clic = 0;   // appuie sur le bouton de l'encoder

#define MODE_MAX 4
int pwm_mode = 0;


///////////////////////////////////////////////// SETUP de BOOT
void setup() {
  // initialize serial communications at 9600 bps:
   Serial.begin(9600);

  // config sortie interrupt de l'inter du l'encodeur ir
  pinMode(CODER_INTER, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(CODER_INTER), coder_inter, CHANGE);
 
  display.begin();
  display.setContrast(60);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 0);
  display.println("  PWR-Unit");
  
  display.drawFastHLine(0, 8, 120, BLACK);  
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 10);
  display.println("  - F4HDG -");
  display.println("    v1.0");
  display.println("--------------");
  display.println("clic to config");
  display.display();

  delay(2000);
  
  /////////////////// clic ? pour menu config
  if (clic)
  {
    delay(200);
    clic = 0;
    menu_config();
  }
}

///////////////////////////////////////////////// LOOOOOOOOOOPPPPPP
void loop() {
  
  switch(pwm_mode)
  {
    case 0 :
      ROSmetre(10);
      break;
    case 1 :
      ROSmetre(100);
      break;
    case 2 :
      PWRmetre(10);
      break;
    case 3 :
      SNAmetre();
      break;
  }
  
  /////////////////// clic ? pour menu config
  if (clic)
  {
    delay(200);
    clic = 0;
    menu_config();
  }
  
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(100);

}

///////////////////////////////////////////////// MODE ROS
void ROSmetre(int moyenne)
{
  int i = 0;
  int RL_ascii = 0;
  unsigned int Analog_dir = 0; // mesure direct
  unsigned int Analog_ret = 0; // mesure retour
  double Pui_dir = 0.0; // Puissance direct
  double Pui_ret = 0.0; // Puissance retour  
  double RL = 1.0;      // return loss
  bool b_Vdir_min = false;
  bool b_Vret_min = false;
  bool b_Vdir_max = false;
  bool b_Vret_max = false;
  
  // read the analog in value:
  Analog_dir = analogRead(DIRECT);
  Analog_ret = analogRead(RETOUR);

  for (i = 0; i < moyenne - 1; i++)
  {
    Analog_dir = (Analog_dir + analogRead(DIRECT));
    Analog_ret = (Analog_ret + analogRead(RETOUR));
  }

  Pui_dir = double(Analog_dir) / double(moyenne);
  Pui_ret = double(Analog_ret) / double(moyenne);

  // map it to the range of the analog out:
  Pui_dir = (Pui_dir * 5.0) / 1023.0;
  Pui_ret = (Pui_ret * 5.0) / 1023.0;
  
  b_Vdir_min = Detecteur_min(Pui_dir);
  b_Vret_min = Detecteur_min(Pui_ret);
  
  b_Vdir_max = Detecteur_max(Pui_dir);
  b_Vret_max = Detecteur_max(Pui_ret);
  
  Pui_dir = Detecteur_V2mW(Pui_dir);
  Pui_ret = Detecteur_V2mW(Pui_ret);
  //~ if(Pui_dir <= detecteur_val)
    //~ Pui_dir = 0;
  //~ if(Pui_ret <= detecteur_val)
    //~ Pui_ret = 0;
    
  

  /* P = Ueff²/R  
   * Ueff  = Umax/sqrt(2)
   * P = Umax² / 2 * R
   * 
   * RL =  sqrt(Pret / Pdir )
   * RL =  sqrt((Vret²/2R) / ((Vdir²/2R))
   * RL = sqrt(Vret² / Vdir²)
   * RL = Vret/Vdir
  
  */

  //~ Pui_dir =  (Pui_dir * Pui_dir) / 100.0;
  //~ Pui_ret =  (Pui_ret * Pui_ret) / 100.0;


  RL = sqrt(Pui_ret/Pui_dir);
  //RL = Pui_ret / Pui_dir;
  RL = (1 + RL) / (1 - RL);

  if (RL > 5)
    RL = 5;
  //////////////////////////////////////DEBUG
//  RL = RL_TEST;
//  if (RL > 5)
//    RL = 5;

  //////////////////////////////////////DEBUG


  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 0);
  display.println("  PWR-Unit");
  display.drawFastHLine(0, 8, 120, BLACK);
  display.drawFastVLine(20, 8, (35-8), BLACK);
  
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 10);
  display.print("Dir ");
  display.print((Pui_dir * pow(10,coupleur_dB/10))/1000.0);
  display.print(" W");

  if (b_Vdir_min)
  {
    display.print(" ");
    display.write(31);
  }

  if (b_Vdir_max)
  {
    display.print(" ");
    display.write(30);
  }
  
  display.println("");
  
  //  display.setCursor(0, 20);
  display.print("Ref ");
  display.print((Pui_ret * pow(10,coupleur_dB/10))/1000.0);
  display.print(" W");
  if (b_Vret_min)
  {
    display.print(" ");
    display.write(31);
  }

  if (b_Vret_max)
  {
    display.print(" ");
    display.write(30);
  }

  display.println("");
  
  //  display.setCursor(0, 30);
  display.print("SWR ");
  if (RL >= 5)
  {
    display.setTextColor(WHITE, BLACK);
    display.print(RL);
    display.print(" ");
    display.write(1);
  }
  else
  {
    display.setTextColor(BLACK, WHITE);
    display.print(RL);
    display.print(" ");
    if (RL >= 2.0)
      display.write(1);
    else
      display.write(2);
  }

  display.println("");

  display.drawFastHLine(0, 35, 120, BLACK);
  display.drawFastVLine(0, 35, 4, BLACK);
  display.drawFastVLine(30, 35, 4, BLACK);
  display.drawFastVLine(60, 35, 4, BLACK);
 
  display.setCursor(0, 40);
  display.setTextColor(BLACK, WHITE);

  if (RL < 2)
  {
    RL_ascii = (RL - 1) * 10;
    for (i = 0 ; i <  RL_ascii; i++)
    {
      if (i < 5)
        display.write(176);
      else
        display.write(177);
    }
  }
  else
  {
    for (i = 0 ; i <  10; i++)
    {
      if (i < 5)
        display.write(176);
      else
        display.write(177);
    }
    RL_ascii = (RL - 1);
    for (i = 0 ; i <  RL_ascii; i++)
      display.write(218);

  }


  display.display();



//  //////////////////////////////////////DEBUG
//  RL_TEST = RL_TEST + 0.1;
//  if (RL_TEST >= 6)
//    RL_TEST = 1;
//  // RL_TEST = double(random(10, 70) / 10.0);

  //////////////////////////////////////DEBUG

}

///////////////////////////////////////////////// MODE SNA
void SNAmetre()
{
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 0);
  display.println("  PWR-Unit");
    
  display.drawFastHLine(0, 8, 120, BLACK);
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 10);
  display.print("Pe ");
  display.println("10 W");
  display.print("Ps ");
  display.println("5 W");
  display.print("G  ");
  display.println("10 dB");
  
  display.display();
}

///////////////////////////////////////////////// MODE powermeter
void PWRmetre(int moyenne)
{
  int i;
  int CAN = 0; // mesure direct
  double analog = 0;
  bool b_Vmin = false;
  bool b_Vmax = false;
  
  // read the analog in value:
  CAN = analogRead(DIRECT);
  
  for (i = 0; i < moyenne - 1; i++)
  {
    CAN = (CAN + analogRead(DIRECT));
  }

  analog = CAN / moyenne;
  
  // map it to the range of the analog out:
  analog = (analog * 5.0) / 1023.0;
  
  b_Vmin = Detecteur_min(analog);
  b_Vmax = Detecteur_max(analog);
  
  
   // correction
  analog = Detecteur_V2mW(analog);
  
  //~ analog = (analog * analog) * 10;
  
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 0);
  display.println("  PWR-Unit");
  
  display.drawFastHLine(0, 8, 120, BLACK);
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0, 10);
  display.print("Puissance W ");
    if (b_Vmin)
  {
    display.print(" ");
    display.write(31);
  }

  if (b_Vmax)
  {
    display.print(" ");
    display.write(30);
  }
  
  display.println("");

  
  
  display.setTextSize(3);
  display.println(analog/1000.0); 

  
  display.display();

}


///////////////////////////////////////////////// DETECTEUR DIODE 1N4148
double Detecteur_V2mW(double tension)
{
  switch (detecteur_type)
  {
    case 0:       // Diode 1N4148
      if (tension <= 0.1)
      {
        tension = -9.7*tension*tension + 3.0*tension + 0.29;
      }
      else 
      {
        tension = (1.078 * tension + 0.42);
      }
      tension = tension*tension * 10;  // V²/(R * 100) * 1000 
      return tension;
   
    case 1 :      // AD3807 log amp
      tension = 40.1 * tension - 87.47;  // tension -> dBm
      tension = pow(10, tension / 10.0); // dBm -> mW
      return tension;
   
    case 2:       // AD8302 gain/phase det
      tension = (1.093 * tension + 0.375);
      tension = tension*tension * 10;  // V²/(R * 100) * 1000 
      return tension;
  }
}

bool Detecteur_min(double tension)
{
   switch (detecteur_type)
  {
    case 0:       // Diode 1N4148
      if (tension >= 0.0)
        return false;
      else 
        return true;
    case 1 :      // AD3807 log amp
      if (tension >= 0.2)
        return false;
      else 
        return true;
    case 2:       // AD8302 gain/phase det
       if (tension >= 0.1)
        return false;
      else 
        return true;
  }
}

bool Detecteur_max(double tension)
{
   switch (detecteur_type)
  {
    case 0:       // Diode 1N4148
      if (tension < 5.0)
        return false;
      else 
        return true;
    case 1 :      // AD3807 log amp
      if (tension < 2.5)
        return false;
      else 
        return true;
    case 2:       // AD8302 gain/phase det
       if (tension < 5.0)
        return false;
      else 
        return true;
  }
}



///////////////////////////////////////////////// MENU de config
void menu_config()
{
  /* IDEE :
   * Mode de mesure ; 
   * coupleur SWR ; detecteur type ; detecteur valeur ; 
   * Moyenne 
   * POWER meter type detecteur 
   * 
   * Acutel
   * 0 Coupleur ; 1 deteceur type ; 2 detecteur value ; 3 Mode ; 4 sortie
   */
  int  config_select = 0; 
  
  char detecteur_type_tab[DETC_MAX][7] = {"Diode\0", "A8307\0", "A8302\0"};
  char pwm_mode_tab[MODE_MAX][4] = {"ROS\0", "MOY\0", "PWR\0", "SNA\0"};
  
  bool sortie_config = false;
  bool refresh = false;

  refresh = true;
  do
  { 
    long myEnc_value = myEnc.read()/4;
    // Mise à jour de l'affichage
    if(refresh == true)
    {
      refresh = false;
      display.clearDisplay();
      display.setTextSize(1);
      display.setTextColor(BLACK);
      display.setCursor(0, 0);
      display.println("  PWR-Unit");
      display.drawFastHLine(0, 8, 120, BLACK);
      display.setTextSize(0);
      display.setTextColor(BLACK);
      display.setCursor(0, 10);
      switch(config_select)
      {
        case 0 :
          display.print(">Coupler ");
          display.println(coupleur_dB);
          display.print(" Det typ ");
          display.println(detecteur_type_tab[detecteur_type]);
          display.print(" Det val ");
          display.println(detecteur_val);
          display.print(" Mode ");
          display.println(pwm_mode_tab[pwm_mode]);
          display.println(" Sortie");
          break;
        case 1 :
          display.print(" Coupler ");
          display.println(coupleur_dB);
          display.print(">Det typ ");
          display.println(detecteur_type_tab[detecteur_type]);
          display.print(" Det val ");
          display.println(detecteur_val);
          display.print(" Mode ");
          display.println(pwm_mode_tab[pwm_mode]);
          display.println(" Sortie");
          break;
        case 2 :
          display.print(" Coupler ");
          display.println(coupleur_dB);
          display.print(" Det typ ");
          display.println(detecteur_type_tab[detecteur_type]);
          display.print(">Det val ");
          display.println(detecteur_val);
          display.print(" Mode ");
          display.println(pwm_mode_tab[pwm_mode]);
          display.println(" Sortie");
          break;
          
        case 3 :
          display.print(" Coupler ");
          display.println(coupleur_dB);
          display.print(" Det typ ");
          display.println(detecteur_type_tab[detecteur_type]);
          display.print(" Det val ");
          display.println(detecteur_val);
          display.print(">Mode ");
          display.println(pwm_mode_tab[pwm_mode]);
          display.println(" Sortie");
          break;
          
        case 4 :
          display.print(" Coupler ");
          display.println(coupleur_dB);
          display.print(" Det typ ");
          display.println(detecteur_type_tab[detecteur_type]);
          display.print(" Det val ");
          display.println(detecteur_val);
          display.print(" Mode ");
          display.println(pwm_mode_tab[pwm_mode]);
          display.println(">Sortie");
          break;
      }
      display.display();
    } 
          
    // changement de config via l'encodeur
    if (myEnc_value_old - myEnc_value != 0)
    {
      switch(config_select)
      {
        // valeur de coupleur
        case 0 :
          coupleur_dB =  coupleur_dB + ((myEnc_value_old - myEnc_value)/10.0);
          break;
        
        case 1 :
          detecteur_type = detecteur_type + (myEnc_value_old - myEnc_value);
          if(detecteur_type > DETC_MAX - 1)
          {
            detecteur_type = 0;
          }
          else if(detecteur_type < 0)
          {
            detecteur_type = DETC_MAX;
          }
          break;
          
        case 2 :
          detecteur_val =  detecteur_val + ((myEnc_value_old - myEnc_value)/100.0);
          break;

        // mode de mesure
        case 3 :
          pwm_mode = pwm_mode + (myEnc_value_old - myEnc_value);
          if(pwm_mode > MODE_MAX - 1)
          {
            pwm_mode = 0;
          }
          else if (pwm_mode < 0)
          {
            pwm_mode = MODE_MAX;
          }
          break;
          
        // Sortie 
        case 4 :
          sortie_config = true;
      }
      myEnc_value_old = myEnc_value;
      refresh = true;
    }
    
    // test clic pour voir si on change de ligne de config
    if(clic)
    {
      config_select =  config_select + 1;
      if(config_select > 4)
      {
        config_select = 0;
      }
      refresh = true;
      clic = 0;
      delay(200);
    }  
  }
  while(sortie_config == false);

}



///////////////////////////////////////////////// Interruption inter encodeur
void coder_inter ()
{
  
  if (digitalRead(CODER_INTER) == 0)  // inter BAS
  {
    clic = 1;
  }
  else                                    // inter HAUT
  {
    Serial.println("INTER HAUT");
  }
}
